#include "Utility.h"
#include <fstream>
#include <sstream>

sLinkedList Utility::add_new_book(sLinkedList lList) {    
    std::string title, author, Isbn;
    int qty;
    std::cout << "Enter title: ";
    getline(std::cin, title);
    std::cout << "Enter author(s): ";
    getline(std::cin, author);
    std::cout << "Enter ISBN: ";
    getline(std::cin, Isbn);
    std::cout << "Enter qty: ";
    std::cin >> qty;
    Book book = Book(title, author, Isbn, qty);
    lList.addLast(book);
    std::ofstream outputFile;
    outputFile.open("books", std::ios::out | std::ios::app);

    outputFile << title << "\t" << author << "\t" << Isbn << "\t" << qty << "\t" << "\n";

    outputFile.close();
    std::cout << "\nThe Book <" << title << "> Added successfully!\n\n";
    return lList;
}

sLinkedList Utility::load_library_data() {
    sLinkedList sList;
    Book* book = new Book();
    std::ifstream inputFile("books");
    std::string bookArray[4];
    std::string line;
    while (getline(inputFile, line)) {
        size_t field2 = 0;
        size_t field1 = line.find("\t");
        for (int i = 0; i <= 3; i++) {
            size_t field1 = line.find("\t" , field2);
            if (field1 != std::string::npos) {
                std::string str2 = line.substr(field2, field1 - field2);
                bookArray[i] = str2;
            }
            field2 = field1 + 1;
        }
        
        book->setTitle(bookArray[0]);
        book->setAuthor(bookArray[1]);
        book->setIsbn(bookArray[2]);
        book->setQty(stoi(bookArray[3]));
        sList.addLast(*book);
    }
    return sList;
}

void Utility::search_book_title(std::string title) {
    Book book;
    sLinkedList myList = load_library_data();
    book = myList.searching_algorihm(title);
    if (!book.getTitle().empty()) {
        std::cout << "\nTitle   : " << book.getTitle() << std::endl;
        std::cout << "Author(s): " << book.getAuthor() << std::endl;
        std::cout << "ISBN    : " << book.getIsbn() << std::endl;
        std::cout << "Qty     : " << book.getQty() << std::endl;
    }
    else {
        std::cout << "\nThe Book: <" << title << "> not found!" << std::endl;
    }
}


sLinkedList Utility::remove_book_by_title(std::string title) {
    Book book;
    sLinkedList myList = load_library_data();

    std::cout << std::endl;
    book = myList.removing_algorithm(title);
        if (!book.getTitle().empty()) {
            std::ofstream outputFile;
            outputFile.open("Books");

            Node* currentNode = myList.get_head();
            while (currentNode != nullptr) {
                std::string title = currentNode->getElement().getTitle();
                std::string authors = currentNode->getElement().getAuthor();
                std::string isbn = currentNode->getElement().getIsbn();
                int qty = currentNode->getElement().getQty();
                outputFile << title << "\t" << authors << "\t" << isbn << "\t" << qty << "\t\n";
                currentNode = currentNode->getNext();
            }
            outputFile.close();

            std::cout << "The Book:[" << book.getTitle() << "] Removed Successfully!\n";
        }
    else
        std::cout << "The Book:[" << title << "] Not Found!\n";
    return myList;
}

void Utility::Evaluate_algorithms_time(sLinkedList s_list) {
    int option;
    std::string selection;
    std::cout << "1- Searching time Evaluate \n";
    std::cout << "2- Deleting time Evaluate  \n";
    std::cout << "\t Insert Your Option: ";

    getline(std::cin, selection);
    option = stoi(selection);

    //int n;
    double time_elapsed = 0;
    clock_t startTime, endTime;

    Book book;
    std::string title = "";
    int books_count = 1, counter = 0, n = 10;
    startTime = clock();
    // get count of the books
    Node* currentNode = s_list.get_head();
    while (currentNode->getNext() != nullptr) {
        currentNode = currentNode->getNext();
        books_count += 1;
    }
    std::string algorithm = "";
    (option == 1) ? algorithm = " // Searching Algorithm" : algorithm = " // Deleting algorithm";

    std::cout << "\nbooks_count = " << books_count << "\n\n";
    std::cout << algorithm << " Time //" << std::endl;
    std::cout << "Book-index \t Time-Elapsed";
    switch (option) {
    case 1: // Searching Evaluate time
        for (int i = 1; i <= books_count; i=i*2) {
            currentNode = s_list.get_head();
            counter = 1;
            if (2 * i >= books_count)
                i = books_count;
            // calculate time for different position of the book
            while (currentNode != nullptr) {
                if (counter == i) {
                    title = currentNode->getElement().getTitle();
                    book = s_list.searching_algorihm(title);
                    endTime = clock();
                    time_elapsed = static_cast<double> (endTime - startTime) / CLOCKS_PER_SEC;
                    std::cout << "\n\t" << counter << "\t\t" << time_elapsed;
                    break;
                }
                counter += 1;
                currentNode = currentNode->getNext();
            }
        }
        break;

    case 2:   // Deleting Evaluate time
        s_list = load_library_data();
        for (int i = 1; i <= books_count; i = i * 2) {
            currentNode = s_list.get_head();
            counter = i;
            if (2 * i >= books_count)
                i = books_count;
            // calculate time for different position of the book
            while (currentNode != nullptr) {
                if (counter == i ) {
                    title = currentNode->getElement().getTitle();
                    s_list.removing_algorithm(title);
                    endTime = clock();
                    time_elapsed = static_cast<double> (endTime - startTime) / CLOCKS_PER_SEC;
                    std::cout << "\n\t" << counter << "\t\t" << time_elapsed;
                    s_list = load_library_data();
                    break;
                }
                counter += 1;
                currentNode = currentNode->getNext();
            }
        }
        break;
    }
}
