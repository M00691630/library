#include <sstream>
#include "sLinkedList.h"
#include "Utility.h"



int main() {
    Utility util;
    // myList is a linkedlist will hold all books from the file
    sLinkedList myList = util.load_library_data();
    int option = 0;
    while (option != 5) {

        std::cout << "\n\n******** The Menu ********\n";


        std::cout << "1.)- Adding a new Book\n";
        std::cout << "2.)- Removing a Book\n";
        std::cout << "3.)- Search Book By title\n";
        std::cout << "4.)- Algorithoms Time Consumed\n";
        std::cout << "5.)- Exit\n";

        std::cout << "\nChoose a Task : ";

        std::cin >> option;
        std::cin.ignore();
        std::string title, author, Isbn;

        switch (option) {
        case 1:   // Adding
            std::cout << "\n******* Adding Book *******\n";
            myList = util.add_new_book(myList);
            break;

        case 2:  // Removing
            std::cout << "\n******* Removing Book *******\n";
            std::cout << "Enter title: ";
            getline(std::cin, title);
            myList = util.remove_book_by_title(title);
            break;

        case 3:   // Searching
            std::cout << "\n******* Searching Book *******\n";
            std::cout << "Enter the title of book to search: ";
            getline(std::cin, title);
            util.search_book_title(title);
            break;

        case 4: // calculate time
            std::cout << "\n******* Evaluate Algorithms Time *******\n";
            util.Evaluate_algorithms_time(myList); 
            break;
        }
    }
}
