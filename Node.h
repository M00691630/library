#ifndef NODE_H
#define NODE_H
#include "Book.h"

class Node {

private:
    Book element;
    Node* next;
public:
    Node(Book element, Node* next);
    Book getElement();
    Node* getNext();
    void setNext(Node* next);

};
#endif 
