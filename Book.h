#ifndef BOOK_H
#define BOOK_H
#include <iostream>


class Book
{
private:
    std::string title;
    std::string author;
    std::string Isbn;
    int qty;
public:

    Book(std::string title, std::string author, std::string Isbn, int qty);
    //Constructor method
    Book();
    //Deconstructor method
    ~Book();

    // Getters methods
    std::string getTitle();
    std::string getAuthor();
    std::string getIsbn();
    int getQty();

    // Setters methods
    void setTitle(std::string title);
    void setAuthor(std::string author);
    void setIsbn(std::string Isbn);
    void setQty(int qty);

};
#endif 
