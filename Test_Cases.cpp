#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "sLinkedList.h"
#include "Utility.h"

Utility util;
// bring all the books into a linkedlist
sLinkedList myList = util.load_library_data();
Book book;
// Testing Search Algorithm
TEST_CASE("testing search result", "[Search]") {

	// assertion with the head of linkedlist
	std::string title = myList.get_head()->getElement().getTitle();
	book = myList.searching_algorihm(title);
	REQUIRE(book.getTitle() == title);
	REQUIRE(book.getTitle() != title + " ");


	//assertion with the tail of linkedlist
	Node* currentNode = myList.get_tail();
	
	title = currentNode->getElement().getTitle();
	book = myList.searching_algorihm(title);

	REQUIRE(book.getTitle() == title);
	REQUIRE(currentNode->getNext() == nullptr);


	// assertion with the another node
	title = myList.get_head()->getNext()->getElement().getTitle();
	book = myList.searching_algorihm(title);
	REQUIRE(book.getTitle() == title);


	//testing the 10th node in linkedlist
	currentNode = myList.get_head();
	for (int i = 1; i <= 10; i++) {
		if(currentNode->getNext() != nullptr)
			currentNode = currentNode->getNext();
	}
	title = currentNode->getElement().getTitle();
	book = myList.searching_algorihm(title);

	REQUIRE(book.getTitle() == title);
	REQUIRE(currentNode->getNext() != nullptr);
}


// Book Class testing
TEST_CASE("Testing Book Class", "[Book()]") {
	Book b1 = myList.get_head()->getElement();
	std::string title = myList.get_head()->getElement().getTitle();
	Book b2 = myList.searching_algorihm(title);
	REQUIRE(b1.getTitle() == b2.getTitle());
	REQUIRE(b1.getAuthor() == b2.getAuthor());
	REQUIRE(b1.getIsbn() == b2.getIsbn());
	REQUIRE(b1.getQty() == b2.getQty());

	Book b3;
	b3.setTitle("Learning ipython");
	b3.setAuthor("jane bond");
	b3.setIsbn("888777000999");
	b3.setQty(6);
	myList.addLast(b3);

	//asertion with book class constructor and searching algorithm
	Book b4 = Book("Learning Java Classes-7", "Oliver", "8181400004213", 9);
	myList.addLast(b4);

	REQUIRE(myList.searching_algorihm("Learning Java Classes-7").getTitle() == "Learning Java Classes-7");
	REQUIRE(myList.searching_algorihm("Learning Java Classes-7").getAuthor() == "Oliver");
	REQUIRE(myList.searching_algorihm("Learning Java Classes-7").getIsbn() == "8181400004213");
	REQUIRE(myList.searching_algorihm("Learning Java Classes-7").getQty() == 9);

	
}

// Test Adding Algorithm
TEST_CASE("Testing Adding Algorithm", "[Adding]") {

	//assertion with last book
	
	Book b = Book("Learning Java Classes-12", "Oliver", "8181400004213", 1);
	myList.addLast(b);
	Node* currentNode = myList.get_tail();
	std::string title = currentNode->getElement().getTitle();
	std::string author = currentNode->getElement().getAuthor();
	std::string isbn = currentNode->getElement().getIsbn();
	int qty = currentNode->getElement().getQty();
	REQUIRE(b.getTitle() == title);
	REQUIRE(b.getAuthor() == author);
	REQUIRE(b.getIsbn() == isbn);
	REQUIRE(b.getQty() == qty);
}

// Test Loading Books
TEST_CASE("Test Loading Data into List", "[sLinkList()]") {
	myList = util.load_library_data();
	// assert that the qty is bigger than zero
	REQUIRE(myList.get_head()->getElement().getQty() > 0);	
	REQUIRE(myList.get_head()->getElement().getAuthor().length() > 0);
	REQUIRE(myList.get_tail()->getElement().getAuthor().length() > 0);
	REQUIRE(myList.get_head()->getNext()->getElement().getTitle() != myList.get_head()->getElement().getTitle());

}
// Testing Remove Algorithm
TEST_CASE("Test Remove Algorithm ", "[Removing]") {
	myList = util.load_library_data();

	// Assertion for Remove last book
	//std::string last_book_title = myList.get_tail()->getElement().getTitle();
	//myList.removing_algorithm(last_book_title);
	//REQUIRE(myList.get_tail()->getElement().getTitle() == last_book_title);


	// Assertion for Remove first book
	Node* h1 = myList.get_head();
	std::string title = h1->getElement().getTitle();
	myList.removing_algorithm(title);
	REQUIRE(myList.get_head()->getElement().getTitle() != title);


}
