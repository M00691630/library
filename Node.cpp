#include "Node.h"

Node::Node(Book element, Node* next) {
    this->element = element;
    this->next = next;
}

Book Node::getElement() { return this->element; }
Node* Node::getNext() { return this->next; }
void Node::setNext(Node* next) { this->next = next; }

