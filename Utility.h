#ifndef UTILITY_H
#define UTILITY_H
//#include <vector>
#include "Node.h"
#include "sLinkedList.h"

class Utility {

private:

public:    
    void search_book_title(std::string title);
    sLinkedList load_library_data();
    sLinkedList add_new_book(sLinkedList lList);
    sLinkedList remove_book_by_title(std::string title);
    void Evaluate_algorithms_time(sLinkedList s_list);

};
#endif 
