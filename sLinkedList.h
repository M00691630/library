#ifndef SLINKEDLIST_H
#define SLINKEDLIST_H
#include "Node.h"

class sLinkedList {

private:
    Node* head = nullptr;
    Node* tail = nullptr;

public:
    Node* get_head();
    Node* get_tail();
    bool isEmpty();
    void addLast(Book item);
    Book removing_algorithm(std::string title);
    Book searching_algorihm(std::string title);

};
#endif 
