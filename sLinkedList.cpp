#include "sLinkedList.h"

Node* sLinkedList::get_head() {
    return this->head;
}

Node* sLinkedList::get_tail() {
    return this->tail;
}

bool sLinkedList::isEmpty() {
    return this->head == nullptr;
}

void sLinkedList::addLast(Book elem) {
    Node* newnode = new Node(elem, nullptr);
    if (this->isEmpty())    
        this->head = newnode;    
    else
        tail->setNext(newnode);
    this->tail = newnode;    
}

Book sLinkedList::searching_algorihm(std::string title) {
    Book b;
    Node* tNode = this->head;

    while (tNode != nullptr) {        
        if (tNode->getElement().getTitle() == title)
            return tNode->getElement();
        tNode = tNode->getNext();
    }
    return b;
}

Book sLinkedList::removing_algorithm(std::string title) {
    Book elem = Book();
    Node* tNode = this->head;
    Node* prevNode = nullptr;
    
    while (tNode->getNext() != nullptr && tNode->getElement().getTitle() != title) {
        prevNode = tNode;
        tNode = tNode->getNext();
    }
    if (tNode == tail && prevNode->getElement().getTitle() != title) {
        delete tNode;
        return elem;
    }
    else if (tNode == head) {
        this->head = this->head->getNext();
    }
    else {
        if (tNode != this->tail)
            prevNode->setNext(tNode->getNext());
        else {
            prevNode->setNext(nullptr);
            this->tail = prevNode;
        }            
    }
    elem = tNode->getElement();
    delete tNode;
    return elem;
}
