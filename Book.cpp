#include "Book.h"

Book::Book(std::string title, std::string author, std::string Isbn, int qty)
{
    this->title = title;
    this->author = author;
    this->Isbn = Isbn;
    this->qty = qty;
}

Book::Book()
{
    title = "";
    author = "";
    Isbn = "";
    qty = 0;
}

Book::~Book()
{
}

// Getters methods
std::string Book::getTitle() { return this->title; }
std::string Book::getAuthor() { return this->author; }
std::string Book::getIsbn() { return this->Isbn; }
int Book::getQty() { return this->qty; }

// Setters methods
void Book::setTitle(std::string title) { this->title = title; }
void Book::setAuthor(std::string author) { this->author = author; }
void Book::setIsbn(std::string Isbn) { this->Isbn = Isbn; }
void Book::setQty(int qty) { this->qty = qty; }
